package com.massivecraft.creativegates.entity.migrator;

import com.massivecraft.creativegates.entity.UConf;
import com.massivecraft.massivecore.store.migrator.MigratorRoot;
import com.massivecraft.massivecore.xlib.gson.JsonElement;
import com.massivecraft.massivecore.xlib.gson.JsonObject;
import com.massivecraft.massivecore.xlib.gson.JsonPrimitive;
import org.bukkit.Bukkit;
import org.bukkit.Material;

import java.util.Map.Entry;

import static org.bukkit.Material.LEGACY_PREFIX;

public class MigratorUConf001TheFlattening extends MigratorRoot
{
	// -------------------------------------------- //
	// INSTANCE & CONSTRUCT
	// -------------------------------------------- //
	
	private static MigratorUConf001TheFlattening i = new MigratorUConf001TheFlattening();
	
	public static MigratorUConf001TheFlattening get()
	{
		return i;
	}
	
	private MigratorUConf001TheFlattening()
	{
		super(UConf.class);
	}
	
	// -------------------------------------------- //
	// CONVERSION
	// -------------------------------------------- //
	
	@Override
	public void migrateInner(JsonObject entity)
	{
		// Get the legacy id
		
		for (String key : new String[]{"materialCreate", "materialInspect", "materialSecret", "materialMode"})
		{
			JsonElement json = entity.get(key);
			if (json == null || json.isJsonNull()) return;
			String legacy = json.getAsString();
			if (!legacy.startsWith(LEGACY_PREFIX))
			{
				legacy = LEGACY_PREFIX + legacy;
			}
			
			Material legacyMaterial = Material.getMaterial(legacy);
			if (legacyMaterial == null) return;
			
			// Get New Material
			try
			{
				Material mat = Bukkit.getUnsafe().fromLegacy(legacyMaterial);
				entity.add(key, new JsonPrimitive(mat.getKey().getKey().toUpperCase()));
			}
			catch (Exception e)
			{
				entity.remove(key);
				System.out.println("Error migrating '" + legacy + "' - Set to Default.");
			}
			
			JsonObject blocksRequired = entity.getAsJsonObject("blocksrequired");
			if (blocksRequired != null && !blocksRequired.isJsonNull())
			{
				for (Entry<String, JsonElement> block : blocksRequired.entrySet())
				{
					legacy = block.getKey();
					if (!legacy.startsWith(LEGACY_PREFIX))
					{
						legacy = LEGACY_PREFIX + legacy;
					}
					legacyMaterial = Material.getMaterial(legacy);
					if (legacyMaterial == null) return;
					
					// Get New Material
					try
					{
						Material mat = Bukkit.getUnsafe().fromLegacy(legacyMaterial);
						blocksRequired.remove(block.getKey());
						blocksRequired.add(mat.getKey().getKey().toUpperCase(), block.getValue());
					}
					catch (Exception e)
					{
						entity.remove("blocksrequired");
						System.out.println("Error migrating blocksrequired - Set to Default.");
						break;
					}
				}
			}
		}
		
	}
}

